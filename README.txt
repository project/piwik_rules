
--------------------------------------------------------------------------------
Piwik Reports
--------------------------------------------------------------------------------

Maintainer:  xlyz, xlyz@tiscali.it

This module adds action to rules to track piwik goals.

Poject homepage: http://drupal.org/project/piwik_rules

Issues: http://drupal.org/project/issues/piwik_rules

Sponsored by Relinc: http://www.relinc.it


Installation
------------

 * Copy the whole piwik_rules directory to your modules directory
   (e.g. DRUPAL_ROOT/sites/all/modules) and activate it in the modules page

 * Copy PiwikTracker.php (available in the Piwik server directory under
   /libs/PiwikTracker)
 
 * Go to configuration page (at admin/config/system/piwik/rules) and set 
   PiwikTracker.php location and a token auth with admin priviledge

 * Add your rules and enjoy ;)

